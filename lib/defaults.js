module.exports = {
	root: true,
	parserOptions: {
		parser: 'babel-eslint',
		sourceType: 'module',
		ecmaVersion: 2018
	},
	env: {
		browser: true,
		es6: true,
		node: true
	},
	extends: [
		'eslint:recommended',
		'prettier',
	],
	plugins: [
		'eslint-plugin-html',
		'prettier',
		'eslint-plugin-import-helpers'
	],

	rules: {
		'accessor-pairs': 'error',
		// Do not use curly braces when not needed for increased readability.
		'arrow-body-style': ['error', 'as-needed'],
		// consistent new line inside arrays
		'array-bracket-newline': 'off',
		// Enforce consistent spacing with brackets (such as arrays).
		'array-element-newline': ['error', 'consistent'],
		// Do not use paranthesises when not needed for increased readability.
		'arrow-parens': ['error', 'as-needed'],
		'arrow-spacing': ['error', { before: true, after: true }],
		'block-spacing': ['error', 'always'],
		'brace-style': 'off',
		// Enforce camelCase
		camelcase: 'error',
		// Use dangling commas when multiline for git commit readability.
		'comma-dangle': ['error', 'only-multiline'],
		// Enforce consistent spacing related to commas.
		'comma-spacing': 'error',
		// Enforce consistent bracket spacing in more places.
		'computed-property-spacing': [
			'error', 'never', {
				enforceForClassMembers: true
			}
		],
		curly: ['error', 'multi-line'],
		'grouped-accessor-pairs': 'error',
		// always newline at EOF
		'eol-last': ['error', 'always'],
		// Consistent spacing when calling functions
		'func-call-spacing': ['error', 'never'],
		'generator-star-spacing': [
			'error', {
				before: true,
				after: false,
				anonymous: 'before',
				method: { before: true, after: true }
			}
		],
		indent: 'off',
		// Space all keys evenly, '[key]: [value]'
		'key-spacing': [
			'error', {
				afterColon: true,
				beforeColon: false,
				mode: 'strict'
			}
		],
		// Space all keywords consistently
		'keyword-spacing': ['error'],
		'no-console': ['warn', { 'allow': ['warn', 'error', 'info', 'assert'] }],
		'no-dupe-else-if': 'error',
		// Chaining the assignment of variables can lead to unexpected results and be difficult to read.
		'no-multi-assign': 'error',
		'no-multi-spaces': 'error',
		'no-multiple-empty-lines': ['error', { max: 2, maxEOF: 1 }],
		'no-tabs': 'off',
		'no-trailing-spaces': 'error',
		// you can use _ or __ as variable names in certain edge cases
		'no-unused-vars': ['error', { 'argsIgnorePattern': '_' }],
		'no-var': 'error',
		'object-curly-newline': 'off',
		// Consistent spacing in objects and arrays
		'object-curly-spacing': [
			'error', 'always', {
				arraysInObjects: true
			}
		],
		'one-var': ['error', 'never'],
		'one-var-declaration-per-line': ['error', 'always'],
		// always newline after a group of variable declarations
		'padding-line-between-statements': [
			'error',
			{
				blankLine: 'always',
				prev: ['const', 'let', 'var'],
				next: '*'
			},
			{
				blankLine: 'any',
				prev: ['const', 'let', 'var'],
				next: ['const', 'let', 'var']
			}
		],
		'prettier/prettier': ['error'],
		// Single quoting for readability and faster typing.
		'quotes': ['error', 'single', { 'avoidEscape': true }],
		// Semi-colons for mitigating risks, readability and consistency.
		semi: ['error', 'always'],
		// No extra spacing before semi-colons but also enforce after.
		'semi-spacing': ['error', { before: false, after: true }],
		'space-before-blocks': ['error', 'always'],
		'space-before-function-paren': [
			'error', {
				anonymous: 'always',
				named: 'never',
				asyncArrow: 'always'
			}
		],
		// Clarify in formatting (intention) for operations with infix
		// operators.
		'space-infix-ops': 'error',
		'space-in-parens': ['error', 'never'],
		'switch-colon-spacing': 'error'
	},
};

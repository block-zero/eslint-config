# Block Zero ESLint Configurations

Opinionated, internal default ESLint config at Block Zero.

## Usage

```bash
npm install -D eslint @block-zero/eslint-config
```

Then, add an ESLint config in the repo, for example, an `.eslintrc.json` with:
```json
{
  "extends": "@block-zero"
}
```

This is the base setup, the project you use it in can overwrite and add things
as needed.
